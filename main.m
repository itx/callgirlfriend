//
//  main.m
//  CallGirlfriend
//
//  Created by Sergej Voronov on 3/15/10.
//  Copyright Sowart 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
    
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    int retVal = UIApplicationMain(argc, argv, nil, nil);
    [pool release];
    return retVal;
}
