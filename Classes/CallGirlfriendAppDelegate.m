//
//  CallGirlfriendAppDelegate.m
//  CallGirlfriend
//
//  Created by Sergej Voronov on 3/15/10.
//  Copyright Sowart 2010. All rights reserved.
//

#import "CallGirlfriendAppDelegate.h"
#import "CallGirlfriendViewController.h"
#include <sys/types.h>
#include <sys/sysctl.h>

@implementation CallGirlfriendAppDelegate

@synthesize window;
@synthesize viewController;


- (void)applicationDidFinishLaunching:(UIApplication *)application {
	buy = false;
	
    size_t size;  
    sysctlbyname("hw.machine", NULL, &size, NULL, 0);  
    char *machine = malloc(size);  
    sysctlbyname("hw.machine", machine, &size, NULL, 0);  
    NSString *platform = [NSString stringWithCString:machine];
    free(machine);  	
	if([platform rangeOfString:@"iPhone"].length == 0)
	{
		[[[UIAlertView alloc] initWithTitle:@"" message:@"iPhone only supported!" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
		return;
	}
		
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSArray *languages = [defaults objectForKey:@"AppleLanguages"];	
	NSString *currentLanguage = [languages objectAtIndex:0];	

	NSString *pnumValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"pnumber_preference"];
	NSString *textValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"ask_preference"];
	NSString *callValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"askcall_preference"];

	NSInteger countValue = [[NSUserDefaults standardUserDefaults] integerForKey:@"count_preference"];
	if(countValue != 0 && countValue % 6 == 0)
	{
		[window addSubview:viewController.view];
		[window makeKeyAndVisible];
		if([currentLanguage isEqualToString:@"ru"] == YES)
			[[[UIAlertView alloc] initWithTitle:@"" message:@"Мужик! Купи оффцу а?!" delegate:self cancelButtonTitle:@"Нет" otherButtonTitles:@"Купить!", nil] show];
		else
			[[[UIAlertView alloc] initWithTitle:@"" message:@"Please buy me-ee-ee?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Buy!", nil] show];
		buy = true;
	}
	else
	{
		if(textValue == nil || pnumValue == nil || [textValue isEqualToString:@"1"] == YES)
		{
			[window addSubview:viewController.view];
			[window makeKeyAndVisible];
		}
		else
		{
			if(callValue == nil || [callValue isEqualToString:@"0"] == YES)
				[self doCall];
			else
			{
				if([currentLanguage isEqualToString:@"ru"] == YES)
					[[[UIAlertView alloc] initWithTitle:@"" message:@"Сделать звонок?" delegate:self cancelButtonTitle:@"Нет" otherButtonTitles:@"Да", nil] show];
				else
					[[[UIAlertView alloc] initWithTitle:@"" message:@"Do you want to call?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil] show];
			}
		}
	}
	countValue++;
	[[NSUserDefaults standardUserDefaults] setInteger:countValue forKey:@"count_preference"];	
}

-(void)doCall
{
	NSString *beValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"be_preference"];
	if(beValue != nil && [beValue isEqualToString:@"1"] == YES)
	{
		NSURL *soundUrl = [NSURL fileURLWithPath: [[NSBundle mainBundle] pathForResource:@"ofca" ofType:@"m4a"]];
		if ( soundUrl )
		{
			s_music = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
			s_music.volume = 1;
			[s_music play];
		}
	}
	
	
	NSString *pnumValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"pnumber_preference"];
	NSString *pnum = [[[pnumValue stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""] stringByReplacingOccurrencesOfString:@" " withString:@""];
	NSString *pnumValue2 = [NSString stringWithFormat:@"tel://%@",pnum];
	NSURL *phoneNumberURL = [NSURL URLWithString:pnumValue2];
	if(![[UIApplication sharedApplication] openURL:phoneNumberURL])
	{
		[[[UIAlertView alloc] initWithTitle:@"ERROR" message:pnumValue delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
	}
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if(!buy)
	{
		if (buttonIndex == 1) 
			[self doCall];
		else
			exit(0);
	}
	else
	{
		buy = false;
		if (buttonIndex == 1) 
		{
			NSURL *url = [NSURL URLWithString:@"http://itunes.apple.com/us/app/call-girlfriend/id362294601?mt=8"];
			[[UIApplication sharedApplication] openURL:url];
		}
		else
		{
			NSString *pnumValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"pnumber_preference"];
			if(pnumValue != nil)
				[self doCall];
		}
	}
}

- (void)dealloc {
    [viewController release];
    [window release];
    [super dealloc];
}


@end
