//
//  CallGirlfriendViewController.h
//  CallGirlfriend
//
//  Created by Sergej Voronov on 3/15/10.
//  Copyright Sowart 2010. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>

@interface CallGirlfriendViewController : UIViewController <ABPeoplePickerNavigationControllerDelegate>{
    IBOutlet UILabel *firstName;	
    IBOutlet UILabel *lastName;
    IBOutlet UILabel *phone;
    IBOutlet UILabel *desc2;
    IBOutlet UISwitch *ask;
    IBOutlet UISwitch *be;
}

@property (nonatomic, retain) UILabel *firstName;
@property (nonatomic, retain) UILabel *lastName;
@property (nonatomic, retain) UILabel *phone;
@property (nonatomic, retain) UILabel *desc2;
@property (nonatomic, retain) UISwitch *ask;
@property (nonatomic, retain) UISwitch *be;

- (IBAction)showPicker:(id)sender;

@end

