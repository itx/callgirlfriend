//
//  CallGirlfriendViewController.m
//  CallGirlfriend
//
//  Created by Sergej Voronov on 3/15/10.
//  Copyright Sowart 2010. All rights reserved.
//

#import "CallGirlfriendViewController.h"

@implementation CallGirlfriendViewController
@synthesize firstName;
@synthesize lastName;
@synthesize phone;
@synthesize desc2;
@synthesize ask;
@synthesize be;

/*
// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];	
	
	NSString *callValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"askcall_preference"];
	if(callValue != nil && [callValue isEqualToString:@"1"] == YES)
		self.ask.on = true;
	else
		self.ask.on = false;
		
	NSString *beValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"be_preference"];
	if(beValue == nil || [beValue isEqualToString:@"1"] == YES)
		self.be.on = true;
	else
		self.be.on = false;
	
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
	
    [firstName release];	
    [lastName release];	
	
    [super dealloc];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:

(UIInterfaceOrientation)interfaceOrientation {
	
    // Return YES for supported orientations
	
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
	
}


- (IBAction)showPicker:(id)sender {
	
    ABPeoplePickerNavigationController *picker =
	
	[[ABPeoplePickerNavigationController alloc] init];
	
    picker.peoplePickerDelegate = self;	
	
    [self presentModalViewController:picker animated:YES];
	
    [picker release];	
}

- (void)peoplePickerNavigationControllerDidCancel:

(ABPeoplePickerNavigationController *)peoplePicker {	
    [self dismissModalViewControllerAnimated:YES];	
}

- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController*)peoplePicker 
      shouldContinueAfterSelectingPerson:(ABRecordRef)person 
{
	[peoplePicker setDisplayedProperties:[NSArray arrayWithObject:[NSNumber numberWithInt:kABPersonPhoneProperty]]];	

    NSString* name = (NSString *)ABRecordCopyValue(person,kABPersonFirstNameProperty);	
    self.firstName.text = name;	
    [name release];		
	
    name = (NSString *)ABRecordCopyValue(person, kABPersonLastNameProperty);	
    self.lastName.text = name;	
    [name release];
	
	return YES;
}

-(BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker
shouldContinueAfterSelectingPerson:(ABRecordRef)person
property:(ABPropertyID)property
identifier:(ABMultiValueIdentifier)identifier{	
	
	ABMultiValueRef phoneProperty = ABRecordCopyValue(person,property);
	NSString *name = (NSString *)ABMultiValueCopyValueAtIndex(phoneProperty,identifier);	
    self.phone.text = name;	
    [name release];
	
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSArray *languages = [defaults objectForKey:@"AppleLanguages"];	
	NSString *currentLanguage = [languages objectAtIndex:0];	
	if([currentLanguage isEqualToString:@"ru"] == YES)
		[[[UIAlertView alloc] initWithTitle:@"" message:@"Теперь вы можете позвонить ей всего лишь касанием картинки\nофцы на вашем телефоне!" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
	else
		[[[UIAlertView alloc] initWithTitle:@"" message:@"Just touch sheep when you want to call your girlfriend!" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];		
		
	[[NSUserDefaults standardUserDefaults] setInteger:self.be.on forKey:@"be_preference"];
	[[NSUserDefaults standardUserDefaults] setInteger:self.ask.on forKey:@"askcall_preference"];	

	[[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"ask_preference"];

	[[NSUserDefaults standardUserDefaults] setValue:self.phone.text forKey:@"pnumber_preference"];
	
    [self dismissModalViewControllerAnimated:YES];
    return NO;	
}



@end










