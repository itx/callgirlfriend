//
//  CallGirlfriendAppDelegate.h
//  CallGirlfriend
//
//  Created by Sergej Voronov on 3/15/10.
//  Copyright Sowart 2010. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@class CallGirlfriendViewController;

@interface CallGirlfriendAppDelegate : NSObject <UIApplicationDelegate,UIAlertViewDelegate> {
    UIWindow *window;
    CallGirlfriendViewController *viewController;
	AVAudioPlayer *s_music;
	bool buy;
}

-(void)doCall;

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet CallGirlfriendViewController *viewController;

@end

